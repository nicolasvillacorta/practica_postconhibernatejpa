package com.captton.heroes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.heroes.model.Heroe;

public interface HeroeDao extends JpaRepository<Heroe, Long> {

	//Defino esta asi puedo usarla en el controller porque no la tengo por default definida.
	public List<Heroe> findByNombre(String nombre);
	
}
