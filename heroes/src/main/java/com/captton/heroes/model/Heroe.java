package com.captton.heroes.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

	
//Le digo al JPA (Hibernate) que esta clase es una entidad (una tabla para base de datos). (import el jpa, no el hibernate)
@Entity
@Table(name="Heroes")
public class Heroe {
	
	// Debo decirle cual es la clave primria
	@Id
	@GeneratedValue
	private Long id;
	
	private String nombre;
	private String bio;
	private String img;
	private String aparicion;
	private String casa;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getBio() {
		return bio;
	}
	public void setBio(String bio) {
		this.bio = bio;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getAparicion() {
		return aparicion;
	}
	public void setAparicion(String aparicion) {
		this.aparicion = aparicion;
	}
	public String getCasa() {
		return casa;
	}
	public void setCasa(String casa) {
		this.casa = casa;
	}
	
}
